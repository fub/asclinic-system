#include "ros/ros.h"
#include <ros/package.h>

#include <string>

// std_msgs
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/UInt32.h"

// Python Messages
#include <asclinic_pkg/ppMessage.h>


// fsmNode stuff
#define NODE_NAME "fsmNode"
#define STATE_TOPIC "fsmStateTopic"
#define NEXT_TARGET_TOPIC "fsmTargetTopic"

// Subscribe to topics
#define TOF_TOPIC "lpfDistance"
#define BOX_TOPIC "itemPresent"
#define ERROR_TOPIC "error"
#define ODOM_TOPIC "odom"

// constants
#define TOF_THRESHOLD 100

// states
#define INIT_STATE 0
#define MOVE 1
#define RECALIBRATE 2
#define TILT_BOX 3
#define GET_NEW_TRAJECTORY 4
#define STOP 5

// current actions
#define DROP 0
#define PICKUP 1

// key locations
#define START_X 1.2
#define START_Y 1.78
#define START_PHI 0

#define DISPENSER_X 0
#define DISPENSER_Y 0
#define DISPENSER_PHI 0

#define RECEPTACLE_X 0
#define RECEPTACLE_Y 0
#define RECEPTACLE_PHI 0

// initialise state

/*
  state = 0:  initial state
  state = 1:  move
  state = 2:  recallibrate
  state = 3:  tilt item holder
*/
int state;

// current action performed
int action;
float tofDistance;
int itemPresent;
float currentPose[3];

// initialise Publisher
ros::Publisher statePublisher;
ros::Publisher nextTargetPublisher;

// variables for pathfinding
float targetPose[3];

// subscriber Callbacks
void tofSubscriberCallback(const std_msgs::Float32& msg);
void itemSubscriberCallback(const std_msgs::UInt32& msg);
void errorSubscriberCallback(const std_msgs::Float32MultiArray& msg);
void odomSubscriberCallback(const std_msgs::Float32MultiArray& msg);

void getNextState();
void stateOutput();

int targetPoseReached();
int checkBox();
void getNewTarget();

// publish to trajectory planner {oldPose, newPose}
void sendNewPoints();

int main(int argc, char* argv[]) {
  // initialise state to 0
  state = 0;
  action = 1;
  itemPresent = 0;

  ros::init(argc,argv,NODE_NAME);
  ros::NodeHandle nodeHandle("~");
  statePublisher = nodeHandle.advertise<std_msgs::Int32>(STATE_TOPIC, 10, false);
  nextTargetPublisher = nodeHandle.advertise<std_msgs::Float32MultiArray>(NEXT_TARGET_TOPIC,10,false);

  // subscribe to all relevant topics
  ros::Subscriber tofSensorSubscriber = nodeHandle.subscribe(TOF_TOPIC, 1, tofSubscriberCallback);
  ros::Subscriber itemSubscriber = nodeHandle.subscribe(BOX_TOPIC,1,itemSubscriberCallback);
  ros::Subscriber errorSubscriber = nodeHandle.subscribe(ERROR_TOPIC,1,errorSubscriberCallback);
  ros::Subscriber odomSubscriber = nodeHandle.subscribe(ODOM_TOPIC,1,odomSubscriberCallback);

  while (ros::ok()){

    ros::spinOnce();
  }
}

void tofSubscriberCallback(const std_msgs::Float32& msg){
  tofDistance = msg.data;
}

void itemSubscriberCallback(const std_msgs::UInt32& msg){
  itemPresent = 0;
}

void errorSubscriberCallback(const std_msgs::Float32MultiArray& msg) {

}

int targetPoseReached(){
  return 0;
}

int checkBox(){
  return 0;
}

void sendNewPoints() {
  std_msgs::Float32MultiArray pts;
  pts.layout.dim.push_back(std_msgs::MultiArrayDimension());
  pts.layout.dim[0].size = 6;
  pts.layout.dim[0].stride = 1;
  pts.layout.dim[0].label = "{currentPose, targetPose}";



}

void getNewTarget(){
  if (action == 1) {
    targetPose[0] = DISPENSER_X;
    targetPose[1] = DISPENSER_Y;
    targetPose[2] = DISPENSER_PHI;
  }

  if (action == 2) {
    targetPose[0] = RECEPTACLE_X;
    targetPose[1] = RECEPTACLE_Y;
    targetPose[2] = RECEPTACLE_PHI;
  }
}

void odomSubscriberCallback(const std_msgs::Float32MultiArray& msg) {
  currentPose[0] = msg.data[0];
  currentPose[1] = msg.data[1];
  currentPose[2] = msg.data[2];
}

void getNextState() {


    if (state == INIT_STATE) {
      // move the robot
      state = MOVE;
    }


    if (state == MOVE) {
      if (tofDistance <= TOF_THRESHOLD) {
        // if current pose the target pose
        // ie robot facing the receptacle or dispenser
        if (targetPoseReached()){
          state = TILT_BOX;
        } else {
          state = RECALIBRATE;
        }
      }

      if (targetPoseReached()) {
        state = TILT_BOX;
      }
    }

    if (state == RECALIBRATE) {
      state = GET_NEW_TRAJECTORY;
    }

    if (state == STOP) {

    }

    if (state == GET_NEW_TRAJECTORY) {

    }
}

void stateOutput() {
  std_msgs::Int32 stateMsg;

  if (state == MOVE) {
    stateMsg.data = state;
    statePublisher.publish(stateMsg);
  }

  else if (state == TILT_BOX) {
    std_msgs::Float32MultiArray newTarget;
    newTarget.layout.dim.push_back(std_msgs::MultiArrayDimension());
    newTarget.layout.dim[0].size = 3;
    newTarget.layout.dim[0].stride = 1;
    newTarget.layout.dim[0].label = "New pose target";

    newTarget.data = {targetPose[0],targetPose[1],targetPose[2]};

    stateMsg.data = state;
    statePublisher.publish(stateMsg);
    getNewTarget();
    nextTargetPublisher.publish(newTarget);
  }

  else if (state == RECALIBRATE) {

  }

  else if (state == GET_NEW_TRAJECTORY) {

  }
}
