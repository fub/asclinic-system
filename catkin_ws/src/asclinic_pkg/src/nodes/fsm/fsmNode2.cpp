// includes
#include <cmath>

// ROS
#include "ros/ros.h"
#include "ros/package.h"

// std_msgs
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/UIn32.h"

// Python Messages
#include <asclinic_pkg/ppMessage.h>

// FSMnode details
#define NODE_NAME "fsmNode"
#define STATE_TOPIC "fsmNodeStateTopic"
#define NEXT_TARGET_TOPIC "fsmTargetTopic"

// Subscribe to the following topics
#define TOF_TOPIC "lpfDistance"
#define BOX_TOPIC "itemPresent"
#define ERROR_TOPIC "error"
#define ODOM_TOPIC "odom"

// Threshold constants
#define TOF_THRESHOLD 100
#define ODOM_ERROR_THRESHOLD_XY 20
#define POSE_THRESHOLD_XY 0.2
#define POSE_THRESHOLD_PHI M_PI*0.2

// states
#define STOP 0
#define MOVE_STRAIGHT 1
#define PURE_ROTATION 2
#define PATHFIND 3
#define TILT_BOX 4

// Current robot action
#define DROP 0
#define PICKUP 1

// key locations
// starting pose
#define START_X 1.2
#define START_Y 1.78
#define START_PHI 0

// dispenser pose
#define DISPENSER_X 0
#define DISPENSER_Y 0
#define DISPENSER_PHI 0

// receptacle pose
#define RECEPTACLE_X 0
#define RECEPTACLE_Y 0
#define RECEPTACLE_PHI 0

// initialise global variables
// current state
int state;

// current action
/*
   0 = move to dispenser
   1 = move to receptacle
 */
int action;

// data
float tofDistance;
int itemPresent;
float odometryError[2];

// pose data
float currentPose[3];
float targetPose[3];
float nextPose[3];

// initialise Publishers
ros::Publisher statePublisher;
ros::Publisher nextTargetPublisher;

// motor coefficients ( to be multiplied with motor speed to set direction)
int coeffLMotor;
int coeffRMotor;

/*               FUNCTIONS DECLARATIONS             */
// Subscriber callback functions
void tofSubscriberCallback(const std_msgs::Float32& msg);
void itemSubscriberCallback(const std_msgs::UInt32& msg);
void errorSubscriberCallback(const std_msgs::Float32MultiArray& msg);
void odomSubscriberCallback(const std_msgs::Float32MultiArray& msg);
void trajectorySubscriberCallback(const asclinic_pkg::ppMessage::ConstPtr& msg);

// State machine functions
void getNextState();
void stateOutput();

// Send initPose and targetPose to path planning FSM
void getPath(float xInit, float yInit, float phiInit, float xTarget, float yTarget, float phiTarget);

// check current pose if it's approximately the the target

int checkXY();
int checkPhi();


/*              main                                 */
int int main(int argc, char const *argv[]) {
  // initialise global variables

  state = 0;
  action = 0;

  // initalise ros nodes
  ros::init(argc,argv,NODE_NAME);
  ros::NodeHandle nh;

  statePublisher = nh.advertise<std_msgs::Int32>(STATE_TOPIC,10,false);
  nextTargetPublisher = nh.advertise<std_msgs::Float32MultiArray>(NEXT_TARGET_TOPIC,10,false);

  ros::Subscriber tofSensorSubscriber = nh.subscribe(TOF_TOPIC, 1, tofSubscriberCallback);
  ros::Subscriber itemSubscriber = nh.subscribe(BOX_TOPIC,1,itemSubscriberCallback);
  ros::Subscriber errorSubscriber = nh.subscribe(ERROR_TOPIC,1,errorSubscriberCallback);
  ros::Subscriber odomSubscriber = nh.subscribe(ODOM_TOPIC,1,odomSubscriberCallback);
  ros::Subscriber trajectorySubscriber = nh.subscribe(TRAJECTORY_TOPIC,1,trajectorySubscriberCallback);

  // get initial path
  getPath(START_X, START_Y, START_PHI, DISPENSER_X, DISPENSER_Y, DISPENSER_PHI);





  while(ros::ok()) {
    ros::spinOnce();
  }
}

void getNextState() {
  switch (state) {
    // current state = STOP
    case STOP:
      if (tofDistance >= TOF_THRESHOLD) {
        state = MOVE_STRAIGHT;
      }

      else {
        state = PATH_FIND;
      }


      break; // end STOP case
    // current state = MOVE_STRAIGHT
    case MOVE_STRAIGHT:
      // if there is an obstacle
      int checkedXY = checkXY();
      int checkedPhi = checkPhi();
      if (tofDistance >= TOF_THRESHOLD) {
        // stop both motors

        state = STOP;
      }

      // if X,Y are correct but phi is not
      if (checkedXY && !checkedPhi) {

        state = PURE_ROTATION;
      }

      if (checkedXY && checkedPhi) {
        state = TILT_BOX;
      }

      if (odometryError[0] >= ODOM_ERROR_THRESHOLD_XY && odometryError[1] >= ODOM_ERROR_THRESHOLD_XY) {
        state = PATHFIND;
      }

      break; // end MOVE_STRAIGHT case
    // current state = PURE_ROTATION
    case PURE_ROTATION:
      int checkedXY = checkXY();
      int checkedPhi = checkPhi();

      if (checkedXY && checkedPhi) {
        state = TILT_BOX;
      }

      if (odometryError[0] >= ODOM_ERROR_THRESHOLD_XY && odometryError[1] >= ODOM_ERROR_THRESHOLD_XY) {
        state = PATHFIND;
      }

      if (!checkedXY && checkedPhi) {
        state = MOVE_STRAIGHT;
      }

      if (tofDistance >= TOF_THRESHOLD) {
        state = STOP;
      }

      break; // end PURE_ROTATION case

    // current state = PATH_FIND
    case PATH_FIND:
      int checkedXY = checkXY();
      int checkedPhi = checkPhi();

      if (checkedXY && !checkedPhi) {
        state = PURE_ROTATION;
      }

      if (!checkedXY && checkedPhi) {
        state = MOVE_STRAIGHT;
      }

      break; // end PATH_FIND case
    // current state = TILT_BOX
    case TILT_BOX:

      state = PATH_FIND;

      break; // end TILT_BOX case



  }

  statePublisher.publish(state)

} // end getNextState

void stateOutput() {
  switch(state) {

    case MOVE_STRAIGHT:
      coeffLMotor = 1;
      coeffRMotor = 1;



      break;

    case PURE_ROTATION:


      break; // end case PURE_ROTATION

    case STOP:

      coeffLMotor = 0;
      coeffRMotor = 0;

      break; // end case STOP

    case PATH_FIND:

      getPath(currentPose[0], currentPose[1], currentPose[2], targetPose[0], targetPose[1], targetPose[2]);

      break; // end case PATH_FIND

    case TILT_BOX:

      break; // end case TILT_BOX

  } // end switch(state)
}

// publishes currentPose and targetPose to be used by path planning node to generate trajectory
void getPath(float xInit, float yInit, float phiInit, float xTarget, float yTarget, float phiTarget) {
  std_msgs::Float32MultiArray pts;
  pts.layout.dim.push_back(std_msgs::MultiArrayDimension());
  pts.layout.dim[0].size = 6;
  pts.layout.dim[0].stride = 1;
  pts.layout.dim[0].label = "{currentPose, targetPose}"

  pts.data = {xInit, yInit, phiInit, xTarget, yTarget, phiTarget};

  nextTargetPublisher.publish(pts);

}

/* checkXY:
            return  0 if X and Y are not within threshold
                    1 if X is within threshold but Y is not
                    2 if Y is within threshold but X is not
                    3 if X and Y are within threshold

*/
int checkXY() {
  int checked = 0

  bool xCheck = currentPose[0] >= nextPose[0] - POSE_THRESHOLD_XY && currentPose[0] <= nextPose[0] + POSE_THRESHOLD_XY;
  bool yCheck = currentPose[1] >= nextPose[1] - POSE_THRESHOLD_XY && currentPose[1] <= nextPose[1] + POSE_THRESHOLD_XY;

  if (xCheck && yCheck) {
    checked = 1;
  }

  return checked;
}

int checkPhi() {
  return currentPose[2] >= nextPose[2] - POSE_THRESHOLD_PHI && currentPose[2] <= nextPose[2] + POSE_THRESHOLD_PHI;
}

/* Subscriber callback functions */

void trajectorySubscriberCallback(const asclinic_pkg::ppMessage::ConstPtr& msg) {

}
