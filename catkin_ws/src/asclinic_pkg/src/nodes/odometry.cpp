#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int16MultiArray.h"
#include <cmath>

/**
 * Subscribing from:
 * *encoder_theta_dot
 * 
 * Publishing to:
 * *odometryPosition
 * 
 * Function:
 * Take the encoder_theta_dot from the encoder output
 * Convert the theta dot into delta theta
 * Estimate the current position
 * Publish the current position to odometryPosition
 * 
 * Loop Rate:
 * Running at 1 loop(s) per second 
*/

// Function prototypes
void callback(const std_msgs::Float32MultiArray::ConstPtr &data);
void trajCallback(const std_msgs::Float32MultiArray::ConstPtr &trajMsg);

// Global constant parameters
const float wheelRadiusLeft = 0.05;
const float wheelRadiusRight = 0.05;
const float halfWheelBase = 0.125;

// Global variables
float xPrevious;
float yPrevious;
float phiPrevious;

bool originInit = false;

std::vector<float> trajectoryX, trajectoryY, trajectoryPhi;
float xFinal, yFinal, xInit, yInit, phiInit;

// Ros global variables
ros::Publisher odometryPublisher;
ros::Publisher controllerStopPublisher;
std_msgs::Float32MultiArray odometryMsg;
std_msgs::Int16MultiArray stopMsg;

/////////////////////////////////Main///////////////////////////////////////////
int main(int argc, char **argv)
{
  // Initializing ros
  ros::init(argc, argv, "odometry");
  ros::NodeHandle n;

  // loop rate
  ros::Rate loop_rate(1);

  // Subscriber
  ros::Subscriber encoder_theta_dot_subscriber = n.subscribe("encoder_theta_dot", 5, callback);
  ros::Subscriber referenceSubscriber = n.subscribe("referenceTrajectory", 1, trajCallback);

  //Publisher
  odometryPublisher = n.advertise<std_msgs::Float32MultiArray>("odometryPosition", 1);
  controllerStopPublisher = n.advertise<std_msgs::Int16MultiArray>("controllerStop", 1, true);

  // Initializing publishing vector for position
  odometryMsg.layout.dim.push_back(std_msgs::MultiArrayDimension());
  odometryMsg.layout.dim[0].size = 3;
  odometryMsg.layout.dim[0].stride = 1;
  odometryMsg.layout.dim[0].label = "position";

  // Initializing publishing vector for controllerStop
  stopMsg.layout.dim.push_back(std_msgs::MultiArrayDimension());
  stopMsg.layout.dim[0].size = 2;
  stopMsg.layout.dim[0].stride = 1;
  stopMsg.layout.dim[0].label = "stop";
  // Main ros loop
  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  } // End of while loop

  return 0;
} // End of main

/////////////////////////////////Functions///////////////////////////////////////////

void callback(const std_msgs::Float32MultiArray::ConstPtr &data)
{
  odometryMsg.data.clear();
  ROS_INFO_STREAM("EEEEEEEEEEEEEEEEEEEEEEEEEEEE");
  ROS_INFO_STREAM("Encoder theta dot data recieved!");
  ROS_INFO_STREAM("Theta Dot Left: " << data->data[0]);
  ROS_INFO_STREAM("Theta Dot Right: " << data->data[1]);

  float deltaThetaLeft, deltaThetaRight, delta_st, delta_phit;
  float thetaDotLeft, thetaDotRight;
  float xP, yP, phi;

  float countInterval = 0.1;

  // Initializing origin position
  if (originInit == false)
  {
    xPrevious = xInit;
    yPrevious = yInit;
    phiPrevious = phiInit;

    originInit = true;
  }
  
  // storing recieved message
  thetaDotLeft = data->data[0];
  thetaDotRight = data->data[1];
  // converting from theta dot to delta theta
  deltaThetaLeft = thetaDotLeft * countInterval;
  deltaThetaRight = thetaDotRight * countInterval;
  ROS_INFO_STREAM("Delta Theta Left: " << deltaThetaLeft);
  ROS_INFO_STREAM("Delta Theta Right: " << deltaThetaRight);

  // Calculating current position
  delta_st = (wheelRadiusLeft * deltaThetaLeft + wheelRadiusRight * deltaThetaRight) / 2;
  delta_phit = (wheelRadiusRight * deltaThetaRight - wheelRadiusLeft * deltaThetaLeft) / (2 * halfWheelBase);

  xP = xPrevious + delta_st * cos(phiPrevious + 0.5 * delta_phit);
  yP = yPrevious + delta_st * sin(phiPrevious + 0.5 * delta_phit);
  phi = phiPrevious + delta_phit;

  // Publishing odometry position data
  odometryMsg.data = {xP, yP, phi};
  odometryPublisher.publish(odometryMsg);
  ROS_INFO_STREAM("=============================");
  ROS_INFO_STREAM("Published xP: " << xP);
  ROS_INFO_STREAM("Published yP: " << yP);
  ROS_INFO_STREAM("Published phi: " << phi);

  if (abs(sqrt((xP - xFinal) * (xP - xFinal) + (yP - yFinal) * (yP - yFinal))) < 0.05)
  {
    stopMsg.data = {1,1};
    controllerStopPublisher.publish(stopMsg);
  }

  ros::spinOnce();
  // Update previous state
  xPrevious = xP;
  yPrevious = yP;
  phiPrevious = phi;
} // End of callback

void trajCallback(const std_msgs::Float32MultiArray::ConstPtr &trajMsg)
{

  ROS_INFO_STREAM("=========================================================");
  ROS_INFO_STREAM("Trajectory information recieved. trajCallback triggered.");

  // get the width and height of the matrix
  ROS_INFO_STREAM("HHHHHHHHHHHHHHHHHHHHHH");
  int trajHeight = trajMsg->layout.dim[0].size;
  int trajWidth = trajMsg->layout.dim[1].size;
  ROS_INFO_STREAM("trajHeight: " << trajHeight);
  ROS_INFO_STREAM("trajWidth: " << trajWidth);

  // Trajectory matrix is 2D
  // Extract each colomn and store in an array
  trajectoryX.resize(trajHeight);
  trajectoryY.resize(trajHeight);

  for (int i = 0; i < trajHeight; i++)
  {
    ROS_INFO_STREAM("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
    trajectoryX[i] = trajMsg->data[i * trajWidth + 0];
    trajectoryY[i] = trajMsg->data[i * trajWidth + 1];
    trajectoryPhi[i] = trajMsg->data[i * trajWidth + 2];
    
  }

  xFinal = trajectoryX[trajHeight - 1];
  yFinal = trajectoryY[trajHeight - 1];

  xInit = trajectoryX[0];
  yInit = trajectoryY[0];
  phiInit = trajectoryPhi[0];

  ROS_INFO_STREAM("x Final position received: " << xFinal);
  ROS_INFO_STREAM("y Final position received: " << yFinal);

  // Print out all the array takes too much effort so...
}