#ifndef MAPPOINTS_H
#define MAPPOINTS_H

class mapPoint {

private:
  float x = 0;
  float y = 0;
  int valid = 0;

public:

  mapPoint(float x, float y, int isValid);

  mapPoint(float x, float y);

  float getX() {return x;}
  float getY() {return y;}
  int getValid() {return valid;}

  void setX(float newX) {x = newX;}
  void setY(float newY) {y = newY;}
  void setValid(int newValid) {valid = newValid;}
};

#endif
