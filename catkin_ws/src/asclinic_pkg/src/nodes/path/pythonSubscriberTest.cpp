#include "ros/ros.h"
#include "ros/package.h"
#include <asclinic_pkg/ppMessage.h>
#include <asclinic_pkg/ppMessageAr.h>

void templateSubscriberCallback(const asclinic_pkg::ppMessageAr::ConstPtr& msg) {

  float nextPose[5];

  nextPose[0] = msg->trajectoryAr[0].trajectory[0];
  nextPose[1] = msg->trajectoryAr[0].trajectory[1];
  nextPose[2] = msg->trajectoryAr[0].trajectory[2];
  nextPose[3] = msg->trajectoryAr[0].trajectory[3];
  nextPose[4] = msg->trajectoryAr[0].trajectory[4];
  ROS_INFO_STREAM("[pythonSubscriberTest] nextPose[0] = " << nextPose[0]);

  for (int i = 0; i < 5; i++) {
    ROS_INFO_STREAM("[pythonSubscriberTest] Value" << nextPose[i]);
  }
}

int main(int argc, char* argv[]) {
  ros::init(argc,argv,"pythonSubscriberTest");
  ros::NodeHandle nh("~");

  ROS_INFO_STREAM("[pythonSubscriberTest] Starting node");

  ros::Subscriber pythonNodeSubscriber = nh.subscribe("/ppNode/pp",1,templateSubscriberCallback);

  ros::spin();
}
