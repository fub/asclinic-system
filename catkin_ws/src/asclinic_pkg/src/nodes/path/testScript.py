from main import pathPlanner
import numpy as np

pp = pathPlanner()


currentLocation = [3,1.0]

endPoint = [0.5,0.5]

ar = pp.getAdjacentPoints(currentLocation)

next = pp.getNextPoint(ar,currentLocation,endPoint)

path = pp.greedyPathfinding(currentLocation,endPoint)

initPose = currentLocation + [np.pi/4]

endPose = endPoint + [np.pi/2]

traj = pp.generateTrajectory(initPose,endPose, path)

print(traj)

for stamp in traj:
    print(stamp)
