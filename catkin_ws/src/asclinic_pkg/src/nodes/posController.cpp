#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/UInt16.h"
#include <cmath>
#include <array>


/**
 * Subscribing from:
 * *posError
 * 
 * Publishing to:
 * *posController_theta_dot
*/

// Function prototypes
void posContollerCallback(const std_msgs::Float32MultiArray::ConstPtr &posErrorMsg);

// Gloval variables
float velocityError, angularError;
ros::Publisher posErrorPublisher;
std_msgs::Float32MultiArray theta_dot;


/////////////////////////////////Main///////////////////////////////////////////
int main(int argc, char **argv)
{
  // Initializing ros
  ros::init(argc, argv, "posController");
  ros::NodeHandle n;
  
  // Subscriber
  ros::Subscriber posErrorSubscriber = n.subscribe("posError", 10, posContollerCallback);

  // Publisher
  posErrorPublisher = n.advertise<std_msgs::Float32MultiArray>("posController_theta_dot", 1000);

  // Initializing publishing vector
  theta_dot.layout.dim.push_back(std_msgs::MultiArrayDimension());
  theta_dot.layout.dim[0].size = 2;
  theta_dot.layout.dim[0].stride = 2;
  theta_dot.layout.dim[0].label = "theta_Dot";

  // loop rate: 1 loop per second
  ros::Rate loop_rate(1);

  // Main ros loop
  while (ros::ok())
  {
    ros::spinOnce();
  } // End of while loop
  loop_rate.sleep();
  return 0;
} // End of main


/////////////////////////////////Functions///////////////////////////////////////////

void posContollerCallback(const std_msgs::Float32MultiArray::ConstPtr &posErrorMsg)
{
  ROS_INFO_STREAM("==============================");
  ROS_INFO_STREAM("Pos Error message recieved!");

  // Storing recieved data
  velocityError = posErrorMsg->data[0];
  angularError = posErrorMsg->data[1];
  ROS_INFO_STREAM("Velocity error recieved: " << velocityError);
  ROS_INFO_STREAM("Angular error recieved: " << angularError);

  // Implementing a P controller
  float pV, pOmega; // Controller output variables
  int kp = 5;

  pV = kp * velocityError;
  pOmega = kp * angularError;

  // Converting controller output to theta dot values
  float theta_l_dot, theta_r_dot;
  float halfWheelBase = 0.125;
  float wheelRadiusLeft = 0.05;
  float wheelRadiusRight = 0.05;
  theta_l_dot = (pV - halfWheelBase * pOmega)/wheelRadiusLeft;
  theta_r_dot = (pV + halfWheelBase * pOmega)/wheelRadiusLeft;  

  // Publish message
  ROS_INFO_STREAM("==============================");
  ROS_INFO_STREAM("Theta L Dot: " << theta_l_dot);
  ROS_INFO_STREAM("Theta R Dot: " << theta_r_dot);
  theta_dot.data = {theta_l_dot, theta_r_dot};
  posErrorPublisher.publish(theta_dot);
  ros::spinOnce();
}