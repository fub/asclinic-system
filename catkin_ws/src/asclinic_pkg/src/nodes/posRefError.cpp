#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/UInt16.h"
#include <cmath>
#include <array>


/**
 * Subscribing topics:
 * *odometryPosition
 * *referenceTrajectory
 * *time
 * 
 * Publishing to:
 * *posError
 * 
 * Functions:
 * Takes the time and referenceTrajectory from matlab
 * Compares the odometry position and current time with the referenceTrajectory
 * Calculates the error in position in terms of 2d distance and angle
 * Out put the position error signals ontp the topic posError
*/

// Function prototypes
void odoCallback(const std_msgs::Float32MultiArray::ConstPtr &odoMsg);
void trajCallback(const std_msgs::Float32MultiArray::ConstPtr &trajMsg);
void currentTimeCallback(const std_msgs::UInt16::ConstPtr &timeMsg);

// Global variables
std::vector<float> trajectoryX, trajectoryY, trajectoryPhi, trajectoryState, trajectoryTime;
float trajectoryLength;
float odometryX, odometryY, odometryPhi;
uint16_t currentTimeMatlab;
int trajHeight, trajWidth;

bool odometryCallback = false;
bool timeInit = false;


/////////////////////////////////Main///////////////////////////////////////////

int main(int argc, char **argv)
{
  // Initialize ros
  ros::init(argc, argv, "posRefError");
  ros::NodeHandle n;

  // Subscribers
  ros::Subscriber odometrySubscriber = n.subscribe("odometryPosition", 1000, odoCallback);
  ros::Subscriber referenceSubscriber = n.subscribe("referenceTrajectory", 1000, trajCallback);
  ros::Subscriber currentTime = n.subscribe("time", 10, currentTimeCallback);

  // Publisher
  ros::Publisher posErrorPublisher = n.advertise<std_msgs::Float32MultiArray>("posError", 10);

  // Initialize publishing vector
  std_msgs::Float32MultiArray posErrorMsg;
  posErrorMsg.layout.dim.push_back(std_msgs::MultiArrayDimension());
  posErrorMsg.layout.dim[0].size = 2;
  posErrorMsg.layout.dim[0].stride = 2;
  posErrorMsg.layout.dim[0].label = "posError";

  // Copying variabled declared in matlab
  float x_p_ref, y_p_ref, phi_ref, error_x, error_y;
  float velocityError, angularError;

  // ros while loop
  while (ros::ok())
  {
    // When odometry data recieved
    while (odometryCallback && timeInit == true)
    {
      ROS_INFO_STREAM("OOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
      ROS_INFO_STREAM("odometry while loop started!");
      int step = 1;
      if (currentTimeMatlab <= trajectoryTime[trajHeight-1])
      {
        ROS_INFO_STREAM("finding index...");
        // Need to performe element wise subtraction
        // Need to find the index of the smallest array element
        float temp = 0; // storing temp calculation for comparison
        int idx = 0;
        for (int i = 0; i < trajHeight; i++)
        {
          // Need to initialize temp
          if (i == 0)
          {
            temp = abs(currentTimeMatlab - trajectoryTime[i]);
            idx = 0;
          }
          else
          {
            float ans = abs(currentTimeMatlab - trajectoryTime[i]);
            if (ans < temp)
            {
              idx = i;
              temp = ans;
            }
          }
        }
        ROS_INFO_STREAM("index found: " << idx);
        
        // Trajectory is the reference
        x_p_ref = trajectoryX[idx];
        y_p_ref = trajectoryY[idx];
        phi_ref = trajectoryPhi[idx];
        ROS_INFO_STREAM("x_p_ref: " << x_p_ref);
        ROS_INFO_STREAM("y_p_ref: " << y_p_ref);
        ROS_INFO_STREAM("phi_p_ref: " << phi_ref);

        // Note: odometry is the current position
        error_x = x_p_ref - odometryX;
        error_y = y_p_ref - odometryY;
        ROS_INFO_STREAM("error_x: " << error_x);
        ROS_INFO_STREAM("error_y: " << error_y);

        if (trajectoryState[idx] == 1)
        {
          velocityError = sqrt(error_x * error_x + error_y * error_y);
          angularError = atan2(error_y, error_x) - odometryPhi;
          ROS_INFO_STREAM("velocity error: " << velocityError);
          ROS_INFO_STREAM("angular error:" << angularError);
        }
        else
        {
          velocityError = 0;
          angularError = phi_ref - odometryPhi;
          ROS_INFO_STREAM("velocity error: " << velocityError);
          ROS_INFO_STREAM("angular error:" << angularError);
        }
      }
      else
      {
        phi_ref = trajectoryPhi[trajHeight - 1];

        velocityError = 0;
        angularError = phi_ref - odometryPhi;
        ROS_INFO_STREAM("velocity error: " << velocityError);
        ROS_INFO_STREAM("angular error:" << angularError);
      }

      // Publish the message
      posErrorMsg.data = {velocityError, angularError};
      posErrorPublisher.publish(posErrorMsg);
      ros::spinOnce();

      // Set the guard back to false and wait for another odometry transmission
      odometryCallback = false;

    } // End of error calculation while loop
  ros::spinOnce();
  }   // End of ros while loop 
  return 0;
}


/////////////////////////////////Functions///////////////////////////////////////////

void odoCallback(const std_msgs::Float32MultiArray::ConstPtr &odoMsg)
{
  ROS_INFO_STREAM("======================================================");
  ROS_INFO_STREAM("Odometry information recieved. odoCallback triggered.");

  // Odometry matrix is 1D
  odometryX = odoMsg->data[0];
  odometryY = odoMsg->data[1];
  odometryPhi = odoMsg->data[2];

  ROS_INFO_STREAM("odometryX: " << odometryX);
  ROS_INFO_STREAM("odometryY: " << odometryY);
  ROS_INFO_STREAM("odometryPhi: " << odometryPhi);

  // Set guard to true
  odometryCallback = true;
}

void trajCallback(const std_msgs::Float32MultiArray::ConstPtr &trajMsg)
{
  ROS_INFO_STREAM("=========================================================");
  ROS_INFO_STREAM("Trajectory information recieved. trajCallback triggered.");

  // get the width and height of the matrix
  ROS_INFO_STREAM("HHHHHHHHHHHHHHHHHHHHHH");
  trajHeight = trajMsg->layout.dim[0].size;
  trajWidth = trajMsg->layout.dim[1].size;
  ROS_INFO_STREAM("trajHeight: " << trajHeight);
  ROS_INFO_STREAM("trajWidth: " << trajWidth);

  // Trajectory matrix is 2D
  // Extract each colomn and store in an array
  trajectoryX.resize(trajHeight);
  trajectoryY.resize(trajHeight);
  trajectoryPhi.resize(trajHeight);
  trajectoryState.resize(trajHeight);
  trajectoryTime.resize(trajHeight);

  for (int i = 0; i < trajHeight; i++)
  {
    ROS_INFO_STREAM("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
    trajectoryX[i] = trajMsg->data[i * trajWidth + 0];
    ROS_INFO_STREAM("trajectoryX["<<i<<"]: " << trajectoryX[i]);
    trajectoryY[i] = trajMsg->data[i * trajWidth + 1];
    ROS_INFO_STREAM("trajectoryY["<<i<<"]: " << trajectoryY[i]);
    trajectoryPhi[i] = trajMsg->data[i * trajWidth + 2];
    ROS_INFO_STREAM("trajectoryPhi["<<i<<"]: " << trajectoryPhi[i]);
    trajectoryState[i] = trajMsg->data[i * trajWidth + 4];
    ROS_INFO_STREAM("trajectoryState["<<i<<"]: " << trajectoryState[i]);
    trajectoryTime[i] = trajMsg->data[i * trajWidth + 3];
    ROS_INFO_STREAM("trajectoryTime["<<i<<"]: " << trajectoryTime[i]);
  }

  // Print out all the array takes too much effort so...
}

void currentTimeCallback(const std_msgs::UInt16::ConstPtr &timeMsg)
{
  currentTimeMatlab = timeMsg->data;
  ROS_INFO_STREAM("==============================");
  ROS_INFO_STREAM("MATLAB current time: " << currentTimeMatlab);
  timeInit = true;
}