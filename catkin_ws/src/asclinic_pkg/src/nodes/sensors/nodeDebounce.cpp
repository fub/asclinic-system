#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt32.h"
#include "std_msgs/Int32.h"
#include <string.h>

#define COUNT 50
#define ALT_COUNT 5

// Name of this node
#define NODE_NAME "/nodeDebounce"

// Name of topic to publish to
#define PUBLISH_TO "debouncedProximity"

// Name of topic to subscribe to
#define SUBSCRIBE_TO "/proximitySensorGroup/proximitySensorNode/proximitySensorEvent"

// Declare templateSubscriberCallback
ros::Publisher debounceEventPublisher;

void subscriberCallback(const std_msgs::Int32& msg) {

  ROS_INFO_STREAM("[DEBOUNCE NODE] MESSAGE RECEIVED WITH VALUE: " << msg.data);

  static int counter = 0;
  static int altCounter = 0;
  static int state = 0;

  if (msg.data == 1-state) {
    ROS_INFO_STREAM("[DEBOUNCE NODE] STATE != DATA --- Incrementing counter");
    counter++;
    altCounter = 0;

  }

  else {
    altCounter++;
  }

  if (altCounter >= ALT_COUNT) {
    counter = 0;
    altCounter = 0;
  }

  if (counter >= COUNT) {
    state = 1 - state;
    ROS_INFO_STREAM("[DEBOUNCE NODE] STATE CHANGE, CURRENT STATE: "<<state);
    counter = 0;
    std_msgs::Int32 stateMsg;
    stateMsg.data = state;
    debounceEventPublisher.publish(stateMsg);
  }
}




int main(int argc, char* argv[]) {

  ros::init(argc, argv, NODE_NAME);

  ros::NodeHandle nodeHandle("~");
  //ros::NodeHandle helpNH("/help");
  debounceEventPublisher = nodeHandle.advertise<std_msgs::Int32>(PUBLISH_TO,10,false);

  ROS_INFO_STREAM("[DEBOUNCE NODE] PUBLISHING TO TOPIC: " << nodeHandle.getNamespace());

  ROS_INFO_STREAM("[DEBOUNCE NODE] NAMESPACE: "<<nodeHandle.getNamespace());

  ros::Subscriber debounceEventSubscriber = nodeHandle.subscribe(SUBSCRIBE_TO,1,subscriberCallback);

  ROS_INFO_STREAM("[DEBOUNCE NODE] SUBSCRIBING TO TOPIC: " << SUBSCRIBE_TO);

  ros::spin();

  return 0;

}
