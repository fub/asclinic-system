/*
  nodeLPF stores buffer from input and applies a moving average filter to the input buffer
*/

#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include <string.h>

#include "boost/bind.hpp"
#include "std_msgs/Float32.h"
#include <cinttypes>

#define BUFFER_SIZE 5

// Node name
#define NODE_NAME "nodeLPF"

// Name of topic to publish to
#define PUBLISH_TO "lpfDistance"

// Name of topic to subscribe to
#define SUBSCRIBE_TO "/ToFSensorGroup/ToFSensorNode/tof_distance"


int buffer[BUFFER_SIZE];

float average;

void initialiseBuffer();
void lpf();
void appendToBuffer(std::uint32_t msg);
void subscriberCallback(const std_msgs::UInt16& msg);

ros::Publisher publisher;

int main(int argc, char* argv[]) {
  average = 0;
  initialiseBuffer();
  ros::init(argc, argv, NODE_NAME);
  ros::NodeHandle nodeHandle("~");
  publisher = nodeHandle.advertise<std_msgs::Float32>("lpfTopic",10,false);
  ros::Subscriber template_subscriber = nodeHandle.subscribe(SUBSCRIBE_TO, 1, subscriberCallback);
  ROS_INFO_STREAM("[LPF NODE] Subscribing to: " << SUBSCRIBE_TO);



  while(ros::ok()) {
    ros::spinOnce();
  }
}

void initialiseBuffer() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
    buffer[i] = 0;
  }
}

void lpf() {
  float sum = 0;

  for(int i = 0; i < BUFFER_SIZE; i++) {
    sum += (float)buffer[i];
    ROS_INFO_STREAM(sum);
  }

  average = sum/BUFFER_SIZE;
}

void appendToBuffer(std::uint32_t data) {
  for (int i = 0; i < BUFFER_SIZE - 1; i++) {
    buffer[i] = buffer[i+1];
  }
  ROS_INFO_STREAM("[LPF NODE] Message data = " <<data);
  buffer[BUFFER_SIZE-1] = data;
}



void subscriberCallback(const std_msgs::UInt16& msg) {
  appendToBuffer((std::uint32_t)msg.data);

  lpf();

  std_msgs::Float32 lpfDist;

  lpfDist.data = average;

  ROS_INFO_STREAM("AVERAGE = " << average);

  publisher.publish(lpfDist);


}
