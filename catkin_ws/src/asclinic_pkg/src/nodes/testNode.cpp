#include "ros/ros.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int32MultiArray.h"
#include "std_msgs/Int16MultiArray.h"
#include "std_msgs/UInt16.h"
#include <cmath>
#include <iostream>
#include <vector>
#include <array>

/**
 * The purpose of this node is to simulate topic inputs 
 * that the physical robot and matlab provides.
 * List of topics that needs to be published:
 * * delta_theta
 * * time
 * * referenceTrajectory
 * 
 * 
 * This node is also used to send test messages to topics
 * to see if they are working correctly.
*/


// Function prototypes
void publishTime(const ros::TimerEvent &evnet);
void stopCallback(const std_msgs::UInt16::ConstPtr &stopMsg);

// Test Time variables
std_msgs::UInt16 currentTimeMessage;
ros::Publisher testPublisherTime;
ros::Timer testTimer;
uint16_t timetime;
ros::Publisher emergencyStopMotor;


int main(int argc, char *argv[])
{
  ros::init(argc, argv, "testNode");
  ros::NodeHandle n;
  ros::Rate loop_rate(5);

  ///////////////////////////////////////////// Test Reference Trajectory ///////////////////////////////////////////////////
  ros::Publisher testReferenceTrajectoryPublisher = n.advertise<std_msgs::Float32MultiArray>("referenceTrajectory", 10, true);

  // Sent guard: only want to send trajectory once
  bool sent = false;

  // Generating fake trajectory
  int width = 5;
  int height = 7;
  float testTrajArray[height][width] = {
      {2.5, 1, 0, 0, 1},
      {2.5, 1.1, 0, 5, 1},
      {2.5, 1.2, 0, 10, 1},
      {2.5, 1.3, 0, 15, 1},
      {2.5, 1.4, 0, 20, 1},
      {2.5, 1.5, 0, 25, 1},
      {2.5, 1.6, 0, 30, 1}};

  // Message to store the test trajectory
  std_msgs::Float32MultiArray testTraj;

  // Initializing the message vector
  testTraj.layout.dim.push_back(std_msgs::MultiArrayDimension());
  testTraj.layout.dim.push_back(std_msgs::MultiArrayDimension());
  testTraj.layout.dim[0].label = "height";
  testTraj.layout.dim[1].label = "width";
  testTraj.layout.dim[0].size = height;
  testTraj.layout.dim[1].size = width;
  testTraj.layout.dim[0].stride = width * height;
  testTraj.layout.dim[1].stride = width;
  testTraj.layout.data_offset = 0;
  std::vector<float> trajVec(width*height, 0);

  // Storing the arra into a tempory vector
  for (int i = 0; i < height; i++)
  {
    for (int j = 0; j < width; j++)
    {
      trajVec[i * width + j] = testTrajArray[i][j];
    }
  }

  // Push the temp vector into the message
  testTraj.data = trajVec;


  //////////////////////////////////////////////// Delta Theta //////////////////////////////////////////////////////////
  ros::Publisher testDeltaThetaPublisher = n.advertise<std_msgs::Float32MultiArray>("encoder_theta_dot", 100, true);

  // Message to store data
  std_msgs::Float32MultiArray deltaTheta;
  deltaTheta.layout.dim.push_back(std_msgs::MultiArrayDimension());
  deltaTheta.layout.dim[0].label = "theta";
  deltaTheta.layout.dim[0].size = 2;
  deltaTheta.layout.dim[0].stride = 2;
  deltaTheta.layout.data_offset = 0;

  // Generating fake data
  float deltaThetaL = 0.89;
  float deltaThetaR = 0.89;

  // Assigning fake data to message
  deltaTheta.data = {deltaThetaL,deltaThetaR};



  ///////////////////////////////////////////////// Test Time ///////////////////////////////////////////////////////////
  testPublisherTime = n.advertise<std_msgs::UInt16>("time", 10);   // Advertiser
  timetime = 0;                                                    // Time variable
  testTimer = n.createTimer(ros::Duration(1), publishTime, false); // Creating timed callback function every 1 sec


  ros::Subscriber emergencyStop = n.subscribe("emergencyStop", 1, stopCallback);
  emergencyStopMotor = n.advertise<std_msgs::Int16MultiArray>("pwm",10);
 /////////////////////////////////////////////////// Main Loop ///////////////////////////////////////////////////////////
  while (ros::ok())
  {
    // Sending the trajectory. Just once.
    if (!sent)
    {
      ROS_INFO_STREAM("===================");
      ROS_INFO_STREAM("trajectory sent!");

      testReferenceTrajectoryPublisher.publish(testTraj);
      sent = true;
    }

    
    // testDeltaThetaPublisher.publish(deltaTheta);

    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}



/////////////////////////////////////////////// Functions and Callbacks ///////////////////////////////////////////////////
void publishTime(const ros::TimerEvent &event)
{
  // Function is called every second. 
  // timetime stores amount of seconds passed.
  currentTimeMessage.data = timetime;
  ++timetime;
  testPublisherTime.publish(currentTimeMessage);
  ROS_INFO_STREAM("==============================");
  ROS_INFO_STREAM("published time: " << timetime);
  ros::spinOnce();
}


void stopCallback(const std_msgs::UInt16::ConstPtr &stopMsg)
{
  if(stopMsg->data == 1){

  std_msgs::Int16MultiArray pwmMsg;

  pwmMsg.layout.dim.push_back(std_msgs::MultiArrayDimension());
  pwmMsg.layout.dim[0].size = 2;
  pwmMsg.layout.dim[0].stride = 2;
  pwmMsg.layout.dim[0].label = "pwm";

  pwmMsg.data =  {0,0};
  emergencyStopMotor.publish(pwmMsg);
  ROS_INFO_STREAM("EMEMMEMEMEMEMMEMEMMEMEEMME");
  ROS_INFO_STREAM("emergency message published!");
  ros::spinOnce();
  }
}