#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt32.h"

// Declare "member" variables
ros::Publisher matlabNodePublisher;
ros::Timer matlabNodeTimerforPublishing;

// Respond to timer callback
void timerCallbackForPublishing(const ros::TimerEvent&)
{
	static uint counter = 0;
	counter++;
	// Publish a message
	std_msgs::UInt32 msg;
	msg.data = counter;
	matlabNodePublisher.publish(msg);
}

// Respond to subscriber receiving a message
void templateSubscriberCallback(const std_msgs::UInt32& msg)
{
	ROS_INFO_STREAM("[testNodeMATLAB] Message receieved with data = " << msg.data);
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "testNodeMATLAB");
	ros::NodeHandle nodeHandle("~");
	// Initialise a publisher
	matlabNodePublisher = nodeHandle.advertise<std_msgs::UInt32>("cppToMatlab", 10, false);
	// Initialise a timer
	matlabNodeTimerforPublishing = nodeHandle.createTimer(ros::Duration(1.0), timerCallbackForPublishing, false);
	// Initialise a subscriber
	ros::Subscriber matlabNodeSubscriber = nodeHandle.subscribe("/MatlabToCPP", 1, templateSubscriberCallback);
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}
