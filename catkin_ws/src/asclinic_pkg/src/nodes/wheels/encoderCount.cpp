#include "ros/ros.h"
#include <ros/package.h>
#include <gpiod.h>
#include "std_msgs/Float32MultiArray.h"

/**
 * Publishing to:
 * *encoder_theta_dot
 * 
 * Function:
 * Takes input from the wheel encoder
 * Use the encoder to work out the current wheel velocity
 * Output the topic onto encoder_theta_dot
*/

// Function prototypes
void timedCountAndPublish(const ros::TimerEvent &evnet);

// ROS variables
ros::Timer timerForCounting;
ros::Publisher delta_theta_publisher;
std_msgs::Float32MultiArray encoderOut;

// Global variables
// Wheel counter
int rightWheelCounter;
int rightWheelCounterPrev;
int leftWheelCounter;
int leftWheelCounterPrev;

double thetaDotLeft, thetaDotRight;
float countInterval;

/////////////////////////////////Main///////////////////////////////////////////
int main(int argc, char **argv)
{
  // Initializing variables
  rightWheelCounter = 0;
  rightWheelCounterPrev = 0;
  leftWheelCounter = 0;
  leftWheelCounterPrev = 0;
  thetaDotLeft = 0;
  thetaDotRight = 0;
  countInterval = 0.1; // How long to count for before publishing

  encoderOut.layout.dim.push_back(std_msgs::MultiArrayDimension());
  encoderOut.layout.dim[0].size = 2;
  encoderOut.layout.dim[0].stride = 2;
  encoderOut.layout.dim[0].label = "encoder";

  // Gpio line number
  int line_number_right_A = 133; // Encoder channel A
  int line_number_right_B = 134; // Encoder channel B
  int line_number_left_A = 105;  // Encoder channel A
  int line_number_left_B = 160;  // Encoder channel B

  // Flags
  int returned_wait_flag_right_A, returned_wait_flag_left_A;
  int returned_wait_flag_right_B, returned_wait_flag_left_B;
  int returned_read_flag_right_A, returned_read_flag_left_A;
  int returned_read_flag_right_B, returned_read_flag_left_B;

  // Current channel B values
  int currentRightB, currentLeftB;

  const char *gpio_chip_name = "/dev/gpiochip0";
  struct gpiod_chip *chip;
  struct gpiod_line *lineLeftA;
  struct gpiod_line *lineLeftB;
  struct gpiod_line *lineRightA;
  struct gpiod_line *lineRightB;

  struct gpiod_line_event eventRightA;
  struct gpiod_line_event eventRightB;
  struct gpiod_line_event eventLeftA;
  struct gpiod_line_event eventLeftB;

  // Initialize ROS
  ros::init(argc, argv, "encoderRight");
  ros::NodeHandle n;
  struct timespec ts = {0, 1000000};

  // Publisher
  delta_theta_publisher = n.advertise<std_msgs::Float32MultiArray>("encoder_theta_dot", 1000);

  // Timer function
  timerForCounting = n.createTimer(ros::Duration(countInterval), timedCountAndPublish, false);

  // Open the gpio chip
  chip = gpiod_chip_open(gpio_chip_name);

  // Initializing each gpio pin
  lineRightA = gpiod_chip_get_line(chip, line_number_right_A);
  lineRightB = gpiod_chip_get_line(chip, line_number_right_B);
  lineLeftA = gpiod_chip_get_line(chip, line_number_left_A);
  lineLeftB = gpiod_chip_get_line(chip, line_number_left_B);

  // Only request falling edge events
  gpiod_line_request_falling_edge_events(lineRightA, "foobar");
  gpiod_line_request_falling_edge_events(lineRightB, "foobar");
  gpiod_line_request_falling_edge_events(lineLeftA, "foobar");
  gpiod_line_request_falling_edge_events(lineLeftB, "foobar");

  /**
     * In this while loop, need to look for falling edges on channel A.
     * When a falling edge is detected, check for the status of channel B to determine wheel rotation direction.
     * Increase a counter each time a falling edge is observed.
    */
  // Ros while loop
  while (ros::ok())
  {
    // Get return wait flags
    returned_wait_flag_right_A = gpiod_line_event_wait(lineRightA, &ts);
    returned_wait_flag_right_B = gpiod_line_event_wait(lineRightB, &ts);
    returned_wait_flag_left_A = gpiod_line_event_wait(lineLeftA, &ts);
    returned_wait_flag_left_B = gpiod_line_event_wait(lineLeftB, &ts);
    ROS_INFO_STREAM("Wait flag RA: " << returned_wait_flag_right_A);
    ROS_INFO_STREAM("Wait flag RB: " << returned_wait_flag_right_B);
    ROS_INFO_STREAM("Wait flag LA: " << returned_wait_flag_left_A);
    ROS_INFO_STREAM("Wait flag LB: " << returned_wait_flag_left_B);

    ///////////////////////////////// Right wheel //////////////////////////////////////////////////////
    // If a falling edge is detected
    if (returned_wait_flag_right_A == 1)
    {
      ROS_INFO_STREAM("RA event detected.");
      // Check if data is valid
      returned_read_flag_right_A = gpiod_line_event_read(lineRightA, &eventRightA);
      returned_read_flag_right_B = gpiod_line_event_read(lineRightB, &eventRightB);
      if (returned_read_flag_right_A == 0 && returned_read_flag_right_B == 0)
      {
        ROS_INFO_STREAM("R event valid.");
        currentRightB = eventRightB.event_type;
        ROS_INFO_STREAM("Event right B: " << currentRightB);
        if (currentRightB == 1) // Wheel is turning clockwise
        {
          rightWheelCounter++;
        }
        else if (currentRightB == 0) // Wheel is turning anti-clockwise
        {
          rightWheelCounter--;
        }
        else if (currentRightB == -1) // Error
        {
          ROS_INFO_STREAM("RB read error!");
        }
      }
    } 

    ///////////////////////////////// Left wheel //////////////////////////////////////////////////////
    // If a falling edge is detected
    if (returned_wait_flag_left_A == 1)
    {
      //ROS_INFO_STREAM("RA event detected.");
      // Check if data is valid
      returned_read_flag_left_A = gpiod_line_event_read(lineLeftA, &eventLeftA);
      returned_read_flag_left_B = gpiod_line_event_read(lineLeftB, &eventLeftB);
      if (returned_read_flag_right_A == 0 && returned_read_flag_right_B == 0)
      {
        //ROS_INFO_STREAM("L event valid.");
        currentLeftB = eventLeftB.event_type;
        ROS_INFO_STREAM("Event left B: " << currentLeftB);
        if (currentLeftB == 0) // Wheel is turning clockwise
        {
          leftWheelCounter++;
        }
        else if (currentLeftB == 1) // Wheel is turning anti-clockwise
        {
          leftWheelCounter--;
        }
        else if (currentLeftB == -1) // Error
        {
          //ROS_INFO_STREAM("LB read error!");
        }
      }
    }
    ros::spinOnce();
  } // end of while loop

  gpiod_chip_close(chip);
  return 0;

} // End of main


/////////////////////////////////////////// Functions ////////////////////////////////////////////////////////

// This function is called every 0.01 seconds
void timedCountAndPublish(const ros::TimerEvent &event)
{
  // Clear the output data
  // encoderOut.data.clear();

  // Values to publish
  thetaDotRight = ((rightWheelCounter - rightWheelCounterPrev) * 360 / 3200 * 3.1415926 / 180) / 0.1;
  thetaDotLeft = ((leftWheelCounter - leftWheelCounterPrev) * 360 / 3200 * 3.1415926 / 180) / 0.1;

  ROS_INFO_STREAM("==============================");
  ROS_INFO_STREAM("Right Wheel Counter: " << rightWheelCounter);
  ROS_INFO_STREAM("Right wheel counter previous: " << rightWheelCounterPrev);
  ROS_INFO_STREAM("Right count difference: " << rightWheelCounter - rightWheelCounterPrev);
  ROS_INFO_STREAM("delta right wheel: " << thetaDotRight);
  ROS_INFO_STREAM("==============================");
  ROS_INFO_STREAM("Left wheel counter: " << leftWheelCounter);
  ROS_INFO_STREAM("Left wheel counter previous: " << leftWheelCounterPrev);
  ROS_INFO_STREAM("Left count difference: " << leftWheelCounter - leftWheelCounterPrev);
  ROS_INFO_STREAM("delta left wheel: " << thetaDotLeft);

  // Update previous value
  rightWheelCounterPrev = rightWheelCounter;
  leftWheelCounterPrev = leftWheelCounter;

  // Publishing data
  encoderOut.data = {float(thetaDotLeft), float(thetaDotRight)};
  delta_theta_publisher.publish(encoderOut);
  ros::spinOnce();
}
