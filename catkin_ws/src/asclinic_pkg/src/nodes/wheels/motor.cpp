#include "ros/ros.h"
#include <ros/package.h>

#include <string.h>
#include <gpiod.h>

#include "std_msgs/UInt16.h"
#include "std_msgs/Int16MultiArray.h"
#include "pololu_smc_g2/pololu_smc_g2.h"
#include "i2c_driver/i2c_driver.h"
#include "std_msgs/Int16MultiArray.h"

/**
 * Subscribing from:
 * *pwm
 * 
 * Function:
 * Set the motor speed in terms of %pwm
*/

// Function prototypes
void pwmCallback(const std_msgs::Int16MultiArray::ConstPtr &pwmMsg);
void initAndDebug(Pololu_SMC_G2 *wheel_pointer, const char *side);
void stopControllerCallback(const std_msgs::Int16MultiArray::ConstPtr &stopControllerMsg);

// Initialize I2C driver
const char *m_i2c_device_name = "/dev/i2c-1";
I2C_Driver m_i2c_driver(m_i2c_device_name);

// Initialize motor drivers
const uint8_t address_wheel_left = 13;
const uint8_t address_wheel_right = 14;

Pololu_SMC_G2 motor_left(&m_i2c_driver, address_wheel_left);
Pololu_SMC_G2 motor_right(&m_i2c_driver, address_wheel_right);

Pololu_SMC_G2 *left_wheel_pointer = &motor_left;
Pololu_SMC_G2 *right_wheel_pointer = &motor_right;

// Stop flag
bool stop;

/////////////////////////////////Main///////////////////////////////////////////
int main(int argc, char *argv[])
{
  // Initialize ros
  ros::init(argc, argv, "motor");
  ros::NodeHandle nodeHandle;
  ros::Rate loop_rate(1);

  // Subscribers
  ros::Subscriber pwmSubscriber = nodeHandle.subscribe("pwm", 1, pwmCallback);
  ros::Subscriber stopcontrollerSubscriber = nodeHandle.subscribe("stopController", 1, stopControllerCallback);

  // I2C init debug
  bool open_success = m_i2c_driver.open_i2c_device();
  if (!open_success)
    ROS_INFO_STREAM("failed to open i2c device");

  // Initialize wheels
  initAndDebug(left_wheel_pointer, "[Left Wheel]: ");
  initAndDebug(right_wheel_pointer, "[Right Wheel]: ");

  stop = false;

  // Main ros loop
  while (ros::ok())
  {
    ros::spinOnce();
    loop_rate.sleep();
  }

  m_i2c_driver.close_i2c_device();

  return 0;
}

/////////////////////////////////Functions///////////////////////////////////////////

void pwmCallback(const std_msgs::Int16MultiArray::ConstPtr &pwmMsg)
{
  bool resultLeft, resultRight;
  if (stop == false)
  {
    ROS_INFO_STREAM("========================");
    ROS_INFO_STREAM("Wheel Controller callback initiated!");
    int pwmLeft = pwmMsg->data[0];
    int pwmRight = pwmMsg->data[1];

    ROS_INFO_STREAM("PWM Left: " << -pwmLeft);
    ROS_INFO_STREAM("PWM Right: " << pwmRight);
    if (pwmLeft < -50)
      pwmLeft = -50;
    if (pwmLeft > 50)
      pwmLeft = 50;

    if (pwmRight < -50)
      pwmRight = -50;
    if (pwmRight > 50)
      pwmRight = 50;

    resultLeft = left_wheel_pointer->set_motor_target_speed_percent(-pwmLeft);
    if (resultLeft)
      ROS_INFO_STREAM("Left Wheel PWM set success! PWM: " << pwmLeft);
    if (!resultLeft)
      ROS_INFO_STREAM("Left Wheel PWM set failed!");

    resultRight = right_wheel_pointer->set_motor_target_speed_percent(pwmRight);
    if (resultRight)
      ROS_INFO_STREAM("Right Wheel PWM set success! PWM: " << pwmRight);
    if (!resultRight)
      ROS_INFO_STREAM("Right Wheel PWM set failed!");
  }
  else if (stop == true)
  {
    resultRight = right_wheel_pointer->set_motor_target_speed_percent(0);
    resultLeft = left_wheel_pointer->set_motor_target_speed_percent(0);
    ROS_INFO_STREAM("Wheels Stoped!");
  }
}

void initAndDebug(Pololu_SMC_G2 *wheel_pointer, const char *side)
{
  // Specify the various limite
  int new_current_limit_in_milliamps = 5000;
  int new_max_speed_limit = 2560;
  int new_max_accel_limit = 1;
  int new_max_decel_limit = 5;

  bool result;
  ROS_INFO_STREAM("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
  ROS_INFO_STREAM("Motors initializing: " << side);

  result = wheel_pointer->exit_safe_start();
  if (!result)
    ROS_INFO_STREAM(side << "failed to exit safe start.");
  if (result)
    ROS_INFO_STREAM(side << "successfuly exited safe start.");

  float input_voltage_value;
  result = wheel_pointer->get_input_voltage_in_volts(&input_voltage_value);
  if (!result)
    ROS_INFO_STREAM(side << "failed to get input voltage value");
  if (result)
    ROS_INFO_STREAM(side << "input voltage value: " << input_voltage_value);

  result = wheel_pointer->set_current_limit_in_milliamps(new_current_limit_in_milliamps);
  if (!result)
    ROS_INFO_STREAM(side << "failed to set new current limit");
  if (result)
    ROS_INFO_STREAM(side << "successfuly set new current limit.");

  usleep(1000); // Short sleep

  int max_speed_limit_response_code;
  result = wheel_pointer->set_motor_limit_max_speed(new_max_speed_limit, &max_speed_limit_response_code);
  if (!result)
    ROS_INFO_STREAM(side << "failed to set max speed limit");
  if (result)
    ROS_INFO_STREAM(side << "successfuly set max speed limit: ." << max_speed_limit_response_code);

  int max_accel_limit_response_code;
  result = wheel_pointer->set_motor_limit_max_acceleration(new_max_accel_limit, &max_accel_limit_response_code);
  if (!result)
    ROS_INFO_STREAM(side << "failed to set max accel limit");
  if (result)
    ROS_INFO_STREAM(side << "successfuly set max accel limit: " << max_accel_limit_response_code);

  int max_decel_limit_response_code;
  result = wheel_pointer->set_motor_limit_max_deceleration(new_max_decel_limit, &max_decel_limit_response_code);
  if (!result)
    ROS_INFO_STREAM(side << "failed to set max decel limit");
  if (result)
    ROS_INFO_STREAM(side << "successfuly set max decel limit: " << max_decel_limit_response_code);
  ROS_INFO_STREAM("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
}

void stopControllerCallback(const std_msgs::Int16MultiArray::ConstPtr &stopControllerMsg)
{
  ROS_INFO_STREAM("!!!!!!!!!!!!!!!!!!!!!!!!!");
  ROS_INFO_STREAM("Stop command recieved!");

  if (stopControllerMsg->data[1] == 1)
  {
    stop = true;
  }
}