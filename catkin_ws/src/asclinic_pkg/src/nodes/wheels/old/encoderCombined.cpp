#include "ros/ros.h"
#include <ros/package.h>
#include <gpiod.h>
#include "std_msgs/Float32MultiArray.h"

int encoderToNum(int A, int B);
int encoderDirectionModifier(int currentNum, int pastNum);
int mapping (int event);

int main(int argc, char **argv)
{
  // Gpio line number
  int line_number_right_A = 105; // Encoder channel A
  int line_number_right_B = 160; // Encoder channel B
  int line_number_left_A = 133;  // Encoder channel A
  int line_number_left_B = 134;  // Encoder channel B

  // Flags
  int returned_wait_flag_right_A, returned_wait_flag_left_A;
  int returned_wait_flag_right_B, returned_wait_flag_left_B;
  int returned_read_flag_right_A, returned_read_flag_left_A;
  int returned_read_flag_right_B, returned_read_flag_left_B;

  int pastEventRightA, pastEventRightB, pastEventLeftA, pastEventLeftB;
  int currentEventRightA, currentEventRightB, currentEventLeftA, currentEventLeftB;
  int rvRightA, rvRightB, rvLeftA, rvLeftB, valueRightA, valueRightB, valueLeftA, valueLeftB;

  const char *gpio_chip_name = "/dev/gpiochip0";
  int num_events_to_display = 20;
  struct gpiod_chip *chip;
  struct gpiod_line *lineLeftA;
  struct gpiod_line *lineLeftB;
  struct gpiod_line *lineRightA;
  struct gpiod_line *lineRightB;

  struct timespec ts = {0, 1000000};
  struct gpiod_line_event eventRightA;
  struct gpiod_line_event eventRightB;
  struct gpiod_line_event eventLeftA;
  struct gpiod_line_event eventLeftB;

  bool initializedRight = false;
  bool initializedLeft = false;

  // Time stamps
  time_t prev_tv_sec_Right_A = -1;
  time_t prev_tv_sec_Right_B = -1;
  time_t prev_tv_sec_Left_A = -1;
  time_t prev_tv_sec_Left_B = -1;
  long int prev_tv_nsec_Right_A = -1;
  long int prev_tv_nsec_Right_B = -1;
  long int prev_tv_nsec_Left_A = -1;
  long int prev_tv_nsec_Left_B = -1;

  // This wheel encoder does 64 Counts Per Revolution (CPR)
  float degreesPerCount = 360 / 3600;
  float radiansPerCount = degreesPerCount * 3.1415926 / 180;

  float deltaThetaLeft, deltaThetaRight;

  // Initialize ROS
  ros::init(argc, argv, "encoderRight");

  valueRightA = gpiod_ctxless_get_value("/dev/gpiochip0", line_number_right_A, false, "foobar");
  valueRightB = gpiod_ctxless_get_value("/dev/gpiochip0", line_number_right_B, false, "foobar");
  valueLeftA = gpiod_ctxless_get_value("/dev/gpiochip0", line_number_left_A, false, "foobar");
  valueLeftB = gpiod_ctxless_get_value("/dev/gpiochip0", line_number_left_B, false, "foobar");

  chip = gpiod_chip_open(gpio_chip_name);

  lineRightA = gpiod_chip_get_line(chip, line_number_right_A);
  lineRightB = gpiod_chip_get_line(chip, line_number_right_B);
  lineLeftA = gpiod_chip_get_line(chip, line_number_left_A);
  lineLeftB = gpiod_chip_get_line(chip, line_number_left_B);

  gpiod_line_request_both_edges_events(lineRightA, "foobar");
  gpiod_line_request_both_edges_events(lineRightB, "foobar");
  gpiod_line_request_both_edges_events(lineLeftA, "foobar");
  gpiod_line_request_both_edges_events(lineLeftB, "foobar");

  ros::NodeHandle n;
  ros::Publisher delta_theta_publisher = n.advertise<std_msgs::Float32MultiArray>("delta_theta", 1000);

  ros::Rate loop_rate(10);

  std_msgs::Float32MultiArray msg;
  msg.layout.dim.push_back(std_msgs::MultiArrayDimension());
  msg.layout.dim[0].size = 2;
  msg.layout.dim[0].stride = 1;
  msg.layout.dim[0].label = "theta";

  while (ros::ok())
  {
    msg.data.clear();
    // Check to see if an event occured
    returned_wait_flag_right_A = gpiod_line_event_wait(lineRightA, &ts);
    returned_wait_flag_right_B = gpiod_line_event_wait(lineRightB, &ts);
    returned_wait_flag_left_A = gpiod_line_event_wait(lineLeftA, &ts);
    returned_wait_flag_left_B = gpiod_line_event_wait(lineLeftB, &ts);

    ////////////////////////////////Right Wheel //////////////////////////////////////////////////////
    // If any channel has an event
    if (returned_wait_flag_right_A || returned_wait_flag_right_B)
    {
      ROS_INFO_STREAM("event detected.");
      // Check if data read was valid
      returned_read_flag_right_A = gpiod_line_event_read(lineRightA, &eventRightA);
      returned_read_flag_right_B = gpiod_line_event_read(lineRightB, &eventRightB);

      if (returned_read_flag_right_A == 0 && returned_read_flag_right_B == 0)
      {
        ROS_INFO_STREAM("reading valid.");
        // If this is the first reading, then initialized
        if (!initializedRight)
        {
          pastEventRightA = eventRightA.event_type;
          pastEventRightB = eventRightB.event_type;

          prev_tv_sec_Right_A = eventRightA.ts.tv_sec;
          prev_tv_nsec_Right_A = eventRightA.ts.tv_nsec;

          prev_tv_sec_Right_B = eventRightB.ts.tv_sec;
          prev_tv_nsec_Right_B = eventRightB.ts.tv_nsec;

          initializedRight = true;
          ROS_INFO_STREAM("right wheel initialized.");

          continue;
        }
        // Normal procedure
        else
        {
          // Get current values of channel A and B
          currentEventRightA = eventRightA.event_type;
          currentEventRightB = eventRightB.event_type;
          ROS_INFO_STREAM("event right A: " << currentEventRightA);
          ROS_INFO_STREAM("event right B: " << currentEventRightB);

          // cast encoder values to a number between 1 and 4 inclusive
          int currentNumRight = encoderToNum(currentEventRightA, currentEventRightB);
          int pastNumRight = encoderToNum(pastEventRightA, pastEventRightB);

          // Compare with past value to see if it moved forwards or backwards
          // The negative here is taking into account left wheel is moving in opposite direction!!
          int modifier = -encoderDirectionModifier(currentNumRight, pastNumRight);

          // Calculate time difference between reading
          float timeDiffSec = float(eventRightA.ts.tv_nsec - prev_tv_nsec_Right_A) / 1000000000;
          float deltaThetaRight = radiansPerCount / timeDiffSec;

          // update past event value
          pastEventRightA = currentEventRightA;
          pastEventRightB = currentEventRightB;
          prev_tv_sec_Right_A = eventRightA.ts.tv_sec;
          prev_tv_nsec_Right_A = eventRightA.ts.tv_nsec;
          prev_tv_sec_Right_B = eventRightB.ts.tv_sec;
          prev_tv_nsec_Right_B = eventRightB.ts.tv_nsec;
        }
      }
    }
    ////////////////////////////////Left Wheel //////////////////////////////////////////////////////
    // If any channel has an event
    if (returned_wait_flag_left_A || returned_wait_flag_left_B)
    {

      // Check if data read was valid
      returned_read_flag_left_A = gpiod_line_event_read(lineLeftA, &eventLeftA);
      returned_read_flag_left_B = gpiod_line_event_read(lineLeftB, &eventLeftB);

      if (returned_read_flag_left_A == 0 && returned_read_flag_left_B == 0)
      {

        // If this is the first reading, then initialized
        if (!initializedLeft)
        {
          pastEventLeftA = eventLeftA.event_type;
          pastEventLeftB = eventLeftB.event_type;

          prev_tv_sec_Left_A = eventLeftA.ts.tv_sec;
          prev_tv_nsec_Left_A = eventLeftA.ts.tv_nsec;

          prev_tv_sec_Left_B = eventLeftB.ts.tv_sec;
          prev_tv_nsec_Left_B = eventLeftB.ts.tv_nsec;

          initializedLeft = true;

          continue;
        }

        // Normal procedure
        else
        {
          // Get current values of channel A and B
          currentEventLeftA = eventLeftA.event_type;
          currentEventLeftB = eventLeftB.event_type;

          // cast encoder values to a number between 1 and 4 inclusive
          int currentNumLeft = encoderToNum(currentEventLeftA, currentEventLeftB);
          int pastNumLeft = encoderToNum(pastEventLeftA, pastEventLeftB);

          // Compare with past value to see if it moved forwards or backwards
          // The negative here is taking into account left wheel is moving in opposite direction!!
          int modifier = -encoderDirectionModifier(currentNumLeft, pastNumLeft);

          // Calculate time difference between reading
          float timeDiffSec = float(eventLeftA.ts.tv_nsec - prev_tv_nsec_Left_A) / 1000000000;
          deltaThetaLeft = radiansPerCount / timeDiffSec;

          // update past event value
          pastEventLeftA = currentEventLeftA;
          pastEventLeftB = currentEventLeftB;
          prev_tv_sec_Left_A = eventLeftA.ts.tv_sec;
          prev_tv_nsec_Left_A = eventLeftA.ts.tv_nsec;
          prev_tv_sec_Left_B = eventLeftB.ts.tv_sec;
          prev_tv_nsec_Left_B = eventLeftB.ts.tv_nsec;
        }
      }
    }
    ////////////////////////////////Send message //////////////////////////////////////////////////////
    msg.data = {deltaThetaLeft,deltaThetaRight};
    delta_theta_publisher.publish(msg);
    ROS_INFO_STREAM("data left wheel: " << deltaThetaLeft);
    ROS_INFO_STREAM("data right wheel: " << deltaThetaRight);
    ros::spinOnce();
    // Close the GPIO chip


  }
    gpiod_chip_close(chip);
    return 0;
}

/**
 * @brief Converts the encovder value from channel A & B to a number between 1 and 4 inclusive
 * @param A Channel A Value
 * @param B Channel B Value
 * @return An int between 1 and 4. Encoder goes 1, 2, 3, 4, 1, 2, 3, 4... if going forwards
 */
int encoderToNum(int A, int B)
{
  int num;

  if (A == 0 && B == 0)
  {
    num = 1;
  }
  else if (A == 1 && B == 0)
  {
    num = 2;
  }
  else if (A == 1 && B == 1)
  {
    num = 3;
  }
  else if (A == 0 && B == 1)
  {
    num = 4;
  }
  return num;
}

/**
 * @brief Give a modifer that tells the direction the wheel is turnning.
 * @param currentNum Current encoder number
 * @param pastNum Previous encoder numver
 * @return 1 if going forwards. -1 if going backwards. 
*/
int encoderDirectionModifier(int currentNum, int pastNum)
{
  if (currentNum == 1 && pastNum == 4)
  {
    return 1;
  }
  else if (currentNum == 4 && pastNum == 1)
  {
    return -1;
  }
  else if (currentNum > pastNum)
  {
    return 1;
  }
  else
  {
    return -1;
  }
}

/**
 * @brief Mapp the event output from 1 and 2 to 0 and 1 accordingly.
 * @param event The event output
 * @return 1 for high, 0 for low
*/
int mapping (int event)
{
  if (event == 2)
  {
    return 0;
  }
  else
  {
    return 1;
  }
}
