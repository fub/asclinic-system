#include "ros/ros.h"
#include <ros/package.h>
#include <gpiod.h>
#include "std_msgs/Float32.h"
#include "std_msgs/Float32MultiArray.h"

int encoderToNum(int A, int B);
int encoderDirectionModifier(int currentNum, int pastNum);

int main(int argc, char **argv)
{
  // Gpio line number
  int line_number_right_A = 105; // Encoder channel A
  int line_number_right_B = 160; // Encoder channel B

  // Flags
  int returned_wait_flag_A;
  int returned_wait_flag_B;
  int returned_read_flag_A;
  int returned_read_flag_B;

  const char *gpio_chip_name = "/dev/gpiochip0";
  int num_events_to_display = 20;
  struct gpiod_chip *chip;
  struct gpiod_line *lineA;
  struct gpiod_line *lineB;

  struct timespec ts = {0, 1000000};
  struct gpiod_line_event eventA;
  struct gpiod_line_event eventB;
  int pastEventA;
  int pastEventB;
  int currentEventA;
  int currentEventB;

  bool initialized = false;

  int rvA, rvB, valueA, valueB;

  // Time stamps
  time_t prev_tv_sec_A = -1;
  time_t prev_tv_sec_B = -1;
  long int prev_tv_nsec_A = -1;
  long int prev_tv_nsec_B = -1;

  // This wheel encoder does 64 Counts Per Revolution (CPR)
  float degreesPerCount = 360 / 64;
  float radiansPerCount = degreesPerCount * 3.1415926 / 180;

  // Initialize ROS
  ros::init(argc, argv, "encoderRight");

  ROS_INFO_STREAM("event variable initialised as: " << eventA.event_type << " timestamp: " << eventA.ts.tv_sec << eventA.ts.tv_nsec);
  printf("event variable initialised as: %d timestamp: [%8ld.%09ld]\n", eventB.event_type, eventB.ts.tv_sec, eventB.ts.tv_nsec);

  valueA = gpiod_ctxless_get_value("/dev/gpiochip0", line_number_right_A, false, "foobar");
  valueB = gpiod_ctxless_get_value("/dev/gpiochip0", line_number_right_B, false, "foobar");

  printf("valueA = %d\n", valueA);
  printf("valueB = %d\n", valueB);

  chip = gpiod_chip_open(gpio_chip_name);

  lineA = gpiod_chip_get_line(chip, line_number_right_A);
  lineB = gpiod_chip_get_line(chip, line_number_right_B);

  printf("Chip %s opened and line %d retrieved\n", gpio_chip_name, line_number_right_A);
  printf("Chip %s opened and line %d retrieved\n", gpio_chip_name, line_number_right_B);

  gpiod_line_request_both_edges_events(lineA, "foobar");
  gpiod_line_request_both_edges_events(lineB, "foobar");

  ros::NodeHandle n;
  ros::Publisher delta_theta_r_publisher = n.advertise<std_msgs::Float32>("delta_theta_r", 1000);

  ros::Rate loop_rate(10);

  while (ros::ok())
  {
  
    // Check to see if an event occured
    returned_wait_flag_A = gpiod_line_event_wait(lineA, &ts);
    returned_wait_flag_B = gpiod_line_event_wait(lineB, &ts);

    // If any channel has an event
    if (returned_wait_flag_A || returned_wait_flag_B)
    {

      // Check if data read was valid
      returned_read_flag_A = gpiod_line_event_read(lineA, &eventA);
      returned_read_flag_B = gpiod_line_event_read(lineB, &eventB);

      if (returned_read_flag_A ==0  && returned_read_flag_B == 0)
      {

        // If this is the first reading, then initialized
        if (!initialized)
        {
          pastEventA = eventA.event_type;
          pastEventB = eventB.event_type;

          prev_tv_sec_A = eventA.ts.tv_sec;
          prev_tv_nsec_A = eventA.ts.tv_nsec;

          prev_tv_sec_B = eventB.ts.tv_sec;
          prev_tv_nsec_B = eventB.ts.tv_nsec;

          initialized = true;

          continue;
        }

        // Normal procedure
        else
        {
          // Get current values of channel A and B
          currentEventA = eventA.event_type;
          currentEventB = eventB.event_type;

          // cast encoder values to a number between 1 and 4 inclusive
          int currentNum = encoderToNum(currentEventA, currentEventB);
          int pastNum = encoderToNum(pastEventA, pastEventB);

          // Compare with past value to see if it moved forwards or backwards
          // The negative here is taking into account left wheel is moving in opposite direction!!
          int modifier = -encoderDirectionModifier(currentNum, pastNum);

          // Calculate time difference between reading
          float timeDiffSec = float(eventA.ts.tv_nsec - prev_tv_nsec_A) / 1000000000;
          float deltaTheta = radiansPerCount/timeDiffSec;
          
          std_msgs::Float32 msg;
          msg.data = deltaTheta;
          delta_theta_r_publisher.publish(msg);
          ROS_INFO_STREAM("delta theta: " << msg.data);
          ros::spinOnce();

          // update past event value
          pastEventA = currentEventA;
          pastEventB = currentEventB;
          prev_tv_sec_A = eventA.ts.tv_sec;
          prev_tv_nsec_A = eventA.ts.tv_nsec;
          prev_tv_sec_B = eventB.ts.tv_sec;
          prev_tv_nsec_B = eventB.ts.tv_nsec;
        }
      }
    }


  }
    gpiod_chip_close(chip);
    return 0;
}

/**
 * @brief Converts the encovder value from channel A & B to a number between 1 and 4 inclusive
 * @param A Channel A Value
 * @param B Channel B Value
 * @return An int between 1 and 4. Encoder goes 1, 2, 3, 4, 1, 2, 3, 4... if going forwards
 */
int encoderToNum(int A, int B)
{
  int num;

  if (A == 0 && B == 0)
  {
    num = 1;
  }
  else if (A == 1 && B == 0)
  {
    num = 2;
  }
  else if (A == 1 && B == 1)
  {
    num = 3;
  }
  else if (A == 0 && B == 1)
  {
    num = 4;
  }
  return num;
}

/**
 * @brief Give a modifer that tells the direction the wheel is turnning.
 * @param currentNum Current encoder number
 * @param pastNum Previous encoder numver
 * @return 1 if going forwards. -1 if going backwards. 
*/
int encoderDirectionModifier(int currentNum, int pastNum)
{
  if (currentNum == 1 && pastNum == 4) 
  {
    return 1;
  }
  else if (currentNum == 4 && pastNum == 1)
  {
    return -1;
  }
  else if (currentNum > pastNum)
  {
    return 1;
  }
  else
  {
    return -1;
  }
}
