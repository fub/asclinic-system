#include "ros/ros.h"
#include <ros/package.h>

#include <string.h>
#include <gpiod.h>

#include "std_msgs/UInt16.h"
#include "std_msgs/Float32MultiArray.h"

#include "pololu_smc_g2/pololu_smc_g2.h"
#include "i2c_driver/i2c_driver.h"


/**
 * Subscribing from:
 * *posController_theta_dot
 * 
 * Publishing to:
 * *pwm
 * 
 * Function:
 * Controls the wheel speed
 * Takes the posController output as the input (i.e. desired velocity)
 * Compare it against the encoderThetaDot (i.e. actual measured velocity)
 * Calculates the error and change the pwm into the motor
*/

// Function prototypes
void leftWheelCallback(const std_msgs::UInt16& msg);
void rightWheelCallback(const std_msgs::UInt16& msg);
void thetaDotCallback(const std_msgs::Float32MultiArray::ConstPtr& thetaDotMsg);
void initAndDebug(Pololu_SMC_G2 * wheel_pointer, const char * side);


// Initialize I2C driver
const char * m_i2c_device_name = "/dev/i2c-1";
I2C_Driver m_i2c_driver (m_i2c_device_name);


// Initialize motor drivers
const uint8_t address_wheel_left = 13;
const uint8_t address_wheel_right = 14;

Pololu_SMC_G2 motor_left (&m_i2c_driver, address_wheel_left);
Pololu_SMC_G2 motor_right (&m_i2c_driver, address_wheel_right);

Pololu_SMC_G2 * left_wheel_pointer = &motor_left;
Pololu_SMC_G2 * right_wheel_pointer = &motor_right;



/////////////////////////////////Main///////////////////////////////////////////
/////////////////////////////////Main///////////////////////////////////////////
/////////////////////////////////Main///////////////////////////////////////////
int main(int argc, char* argv[])
{
    // Initialize ros
    ros::init(argc, argv, "controller");
    ros::NodeHandle nodeHandle;
    ros::Rate loop_rate(50);

    // Subscribers
    //ros::Subscriber left_wheel_subscriber = nodeHandle.subscribe("theta_dot_l", 1, leftWheelCallback);
    //ros::Subscriber right_wheel_subscriber = nodeHandle.subscribe("theta_dot_r", 1, rightWheelCallback);
    ros::Subscriber theta_dot_subscriber = nodeHandle.subscribe("posController_theta_dot", 1, thetaDotCallback);
    // I2C init debug
    bool open_success = m_i2c_driver.open_i2c_device();
    if (!open_success) ROS_INFO_STREAM("failed to open i2c device");

    // Initialize wheels
    initAndDebug(left_wheel_pointer, "[Left Wheel]: ");
    initAndDebug(right_wheel_pointer, "[Right Wheel]: ");

    // Main ros loop
    while (ros::ok())
    {
        ros::spin();
        loop_rate.sleep();

    }

    m_i2c_driver.close_i2c_device();

    return 0;
}




/////////////////////////////////Functions///////////////////////////////////////////
/////////////////////////////////Functions///////////////////////////////////////////
/////////////////////////////////Functions///////////////////////////////////////////

void leftWheelCallback(const std_msgs::UInt16& msg)
{
    bool result;
    ROS_INFO_STREAM("[Left Wheel]: callback initiated");
    ROS_INFO_STREAM("[Left Wheel]: theta_dot_l data recieved: " <<msg.data);
 
    int kp = 1;
    int pwm_duty_cycle = - kp * int(msg.data);
    ROS_INFO_STREAM("[Left Wheel] controller pwm cycle: " << pwm_duty_cycle);

    // Clamp the pwm output to be between 0 and 100
    if (pwm_duty_cycle < -100)
		pwm_duty_cycle = -100;

	if (pwm_duty_cycle > 100)
		pwm_duty_cycle = 100;

    ROS_INFO_STREAM("[Left Wheel]: actual pwm set to: " << pwm_duty_cycle);

    result = left_wheel_pointer->set_motor_target_speed_percent(pwm_duty_cycle);
    if(!result) ROS_INFO_STREAM("[Left Wheel]: failed to set pwm.");
    if(result) ROS_INFO_STREAM("[Left Wheel]: set pwm success!");
}


void rightWheelCallback(const std_msgs::UInt16& msg)
{
    bool result;
    ROS_INFO_STREAM("[Right Wheel]: callback initiated");
    ROS_INFO_STREAM("[Right Wheel]: theta_dot_l data recieved: " <<msg.data);
 
    int kp = 1;
    int pwm_duty_cycle = kp * int(msg.data);
    ROS_INFO_STREAM("[Right Wheel] controller pwm cycle: " << pwm_duty_cycle);

    // Clamp the pwm output to be between 0 and 100
    if (pwm_duty_cycle < -100)
		pwm_duty_cycle = -100;

	if (pwm_duty_cycle > 100)
		pwm_duty_cycle = 100;

    ROS_INFO_STREAM("[Right Wheel]: actual pwm set to: " << pwm_duty_cycle);

    result = right_wheel_pointer->set_motor_target_speed_percent(pwm_duty_cycle);
    if(!result) ROS_INFO_STREAM("[Right Wheel]: failed to set pwm.");
    if(result) ROS_INFO_STREAM("[Right Wheel]: set pwm success!");
}


void thetaDotCallback(const std_msgs::Float32MultiArray::ConstPtr& thetaDotMsg)
{
    bool resultLeft, resultRight;
    ROS_INFO_STREAM("========================");
    ROS_INFO_STREAM("Wheel Controller callback initiated!");
    float thetaDotL = thetaDotMsg->data[0];
    float thetaDotR = thetaDotMsg->data[1];

    ROS_INFO_STREAM("Theta Dot L: " << thetaDotL);
    ROS_INFO_STREAM("Theta Dot R: " << thetaDotR);

    int kp = 1;

    int pwmLeft = - kp * thetaDotL;
    if(pwmLeft < -100) pwmLeft = -100;
    if(pwmLeft > 100) pwmLeft = 100;
    ROS_INFO_STREAM("PWM Left set to: " << pwmLeft);

    int pwmRight = kp * thetaDotR;
    if(pwmRight < -100) pwmRight = -100;
    if(pwmRight > 100) pwmRight = 100;
    ROS_INFO_STREAM("PWM Right set to: " << pwmRight);

    resultLeft = left_wheel_pointer->set_motor_target_speed_percent(pwmLeft);
    if (resultLeft) ROS_INFO_STREAM("Left Wheel PWM set success!");
    if (!resultLeft) ROS_INFO_STREAM("Left Wheel PWM set failed!");

    resultRight = right_wheel_pointer->set_motor_target_speed_percent(pwmRight);
    if (resultRight) ROS_INFO_STREAM("Right Wheel PWM set success!");
    if (!resultRight) ROS_INFO_STREAM("Right Wheel PWM set failed!");
}


void initAndDebug(Pololu_SMC_G2 * wheel_pointer, const char * side)
{
    // Specify the various limite
	int new_current_limit_in_milliamps = 5000;
	int new_max_speed_limit = 2560;
	int new_max_accel_limit = 1;
	int new_max_decel_limit = 5;

    bool result;

    ROS_INFO_STREAM("Now initializing: " << side);

    result = wheel_pointer->exit_safe_start();
    if(!result) ROS_INFO_STREAM(side << "failed to exit safe start.");
    if(result) ROS_INFO_STREAM(side << "successfuly exited safe start.");

    float input_voltage_value;
    result = wheel_pointer->get_input_voltage_in_volts(&input_voltage_value);
    if(!result) ROS_INFO_STREAM(side << "failed to get input voltage value");
    if(result) ROS_INFO_STREAM(side << "input voltage value: " << input_voltage_value);

    result = wheel_pointer->set_current_limit_in_milliamps(new_current_limit_in_milliamps);
    if(!result) ROS_INFO_STREAM(side << "failed to set new current limit");
	if(result) ROS_INFO_STREAM(side << "successfuly set new current limit.");

	usleep(1000);   // Short sleep

    int max_speed_limit_response_code;
    result = wheel_pointer->set_motor_limit_max_speed(new_max_speed_limit, &max_speed_limit_response_code);
    if(!result) ROS_INFO_STREAM(side << "failed to set max speed limit");
    if(result) ROS_INFO_STREAM(side << "successfuly set max speed limit: ." << max_speed_limit_response_code);

    int max_accel_limit_response_code;
	result = wheel_pointer->set_motor_limit_max_acceleration(new_max_accel_limit, &max_accel_limit_response_code);
    if(!result) ROS_INFO_STREAM(side << "failed to set max accel limit");
    if(result) ROS_INFO_STREAM(side << "successfuly set max accel limit: " << max_accel_limit_response_code);

    int max_decel_limit_response_code;
	result = wheel_pointer->set_motor_limit_max_deceleration(new_max_decel_limit, &max_decel_limit_response_code);
    if(!result) ROS_INFO_STREAM(side << "failed to set max decel limit");
    if(result) ROS_INFO_STREAM(side << "successfuly set max decel limit: " << max_decel_limit_response_code);
}

