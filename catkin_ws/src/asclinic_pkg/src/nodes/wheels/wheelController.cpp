#include "ros/ros.h"
#include <ros/package.h>

#include <string.h>

#include "std_msgs/UInt16.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int16MultiArray.h"

/**
 * Subscribing from:
 * *posController_theta_dot
 * 
 * Publishing to:
 * *pwm
 * 
 * Function:
 * Controls the wheel speed
 * Takes the posController output as the input (i.e. desired velocity)
 * Compare it against the encoderThetaDot (i.e. actual measured velocity)
 * Calculates the error and change the pwm into the motor
*/

// Function prototypes
void refThetaDotCallback(const std_msgs::Float32MultiArray::ConstPtr &posThetaDotMsg);
void encoderThetaDotCallback(const std_msgs::Float32MultiArray::ConstPtr &encoderThetaDotMsg);
// Global Variables
std_msgs::Int16MultiArray pwmMsg;
bool posFlag, encoderFlag;
float refThetaDotLeft, refThetaDotRight;
float encoderThetaDotLeft, encoderThetaDotRight;

/////////////////////////////////Main///////////////////////////////////////////
int main(int argc, char *argv[])
{
  // Initialize variables
  posFlag = false;     // Initialize flag to false
  encoderFlag = false; // Initialize flag to false
  int prevPWMLeft = 0; // Storing previous pwm value
  int prevPWMRight = 0;
  int kP = 1;                // Proportional controller
  int16_t pwmLeft, pwmRight; // Storing calculated pwm value
  float errorLeft = 0;       // Error
  float errorRight = 0;
  float tempLeft = 0;
  float tempRight = 0;

  //Initializing publishing message vector
  pwmMsg.layout.dim.push_back(std_msgs::MultiArrayDimension());
  pwmMsg.layout.dim[0].size = 2;
  pwmMsg.layout.dim[0].stride = 2;
  pwmMsg.layout.dim[0].label = "pwm";

  // Initialize ros
  ros::init(argc, argv, "controller");
  ros::NodeHandle nodeHandle;
  ros::Rate loop_rate(50);

  // Subscriber
  ros::Subscriber posController_theta_dot_subscriber = nodeHandle.subscribe("posController_theta_dot", 10, refThetaDotCallback);
  ros::Subscriber encoder_theta_dot_subscriber = nodeHandle.subscribe("encoder_theta_dot", 10, encoderThetaDotCallback);
  // Publisher
  ros::Publisher pwmPublisher = nodeHandle.advertise<std_msgs::Int16MultiArray>("pwm", 10);

  // Main ros loop
  while (ros::ok())
  {
    if (posFlag == true && encoderFlag == true)
    {
      ROS_INFO_STREAM("==========================");
      ROS_INFO_STREAM("Controller started!");

      // Calculated the error
      errorLeft = refThetaDotLeft - encoderThetaDotLeft;
      errorRight = refThetaDotRight - encoderThetaDotRight;

      // Controller output stored in temp variable
      tempLeft = kP * errorLeft;
      tempRight = kP * errorRight;

      // New pwm is modified based on previous sent PWM
      pwmLeft = int16_t(prevPWMLeft + tempLeft);
      pwmRight = int16_t(prevPWMRight + tempRight);

      // Publish message
      pwmMsg.data = {pwmLeft, pwmRight};

      pwmPublisher.publish(pwmMsg);
      ROS_INFO_STREAM("PWM values published!");
      ROS_INFO_STREAM("PWM Left: " << pwmLeft);
      ROS_INFO_STREAM("PWM Right: " << pwmRight);

      // Set flags back to flase
      posFlag = false;
      encoderFlag = false;

      prevPWMLeft = pwmLeft;
      prevPWMRight = pwmRight;
    } // End of if statement

    ros::spinOnce();
  } // End of ros while loop

  return 0;
} // End of main

/////////////////////////////////Functions///////////////////////////////////////////

void refThetaDotCallback(const std_msgs::Float32MultiArray::ConstPtr &posThetaDotMsg)
{
  // Callback initiated
  ROS_INFO_STREAM("RRRRRRRRRRRRRRRRRRRRRRRRRRR");
  ROS_INFO_STREAM("Reference Theta Dot recieved!");

  // Store recieved values
  refThetaDotLeft = posThetaDotMsg->data[0];
  refThetaDotRight = posThetaDotMsg->data[1];
  ROS_INFO_STREAM("Reference Theta Dot Left: " << refThetaDotLeft);
  ROS_INFO_STREAM("Reference Theta Dot Right: " << refThetaDotRight);

  // Set flag to true
  posFlag = true;
} // End of refThetaDotCallback

void encoderThetaDotCallback(const std_msgs::Float32MultiArray::ConstPtr &encoderThetaDotMsg)
{
  // Callback initiated
  ROS_INFO_STREAM("EEEEEEEEEEEEEEEEEEEEEEEEEEE");
  ROS_INFO_STREAM("Encoder Theta Dot recieved!");

  // Store recieved values
  encoderThetaDotLeft = encoderThetaDotMsg->data[0];
  encoderThetaDotRight = encoderThetaDotMsg->data[1];
  ROS_INFO_STREAM("Encoder Theta Dot Left: " << encoderThetaDotLeft);
  ROS_INFO_STREAM("Encoder Theta Dot Right: " << encoderThetaDotRight);

  // Set flag to true
  encoderFlag = true;
} // End of encoderThetaDotCallback
